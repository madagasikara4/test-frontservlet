package models;

import genericservlet.ModelView;
import genericservlet.UrlServlet;

import java.text.SimpleDateFormat;
import java.util.Date;

public class Employer {
    private int id;
    private String nom;
    private String prenom;
    private Date dateanniv;
    private String[] diplome;

    public Employer(){}

    public Employer(int id, String nom, String prenom, Date dateanniv,String[] diplome){
        this.id=id;
        this.dateanniv=dateanniv;
        this.nom=nom;
        this.prenom=prenom;
        this.diplome=diplome;
    }

    @UrlServlet(url = "list-emp")
    public static ModelView findAll() throws Exception{
        Employer[] d=new Employer[3];
        String[] diplome=new String[2];
        diplome[0]="Bacc";
        diplome[1]="License";
        d[0]=new Employer(1,"Rakoto","Mark",new SimpleDateFormat("dd/MM/yyyy").parse("12/11/2002"),diplome);
        d[1]=new Employer(2,"Rabe","Solo",new SimpleDateFormat("dd/MM/yyyy").parse("01/06/2000"),diplome);
        d[2]=new Employer(3,"Rafetra","Koto",new SimpleDateFormat("dd/MM/yyyy").parse("26/07/1997"),diplome);

        ModelView result=new ModelView("Emp",d,"listeEmployer.jsp");
        return result;
    }

    @UrlServlet(url="ajout-emp")
    public ModelView listeEmployer()throws Exception{
        Employer[] d=new Employer[4];
        String[] diplome=new String[2];
        diplome[0]="Bacc";
        diplome[1]="License";
        d[0]=new Employer(1,"Rakoto","Mark",new SimpleDateFormat("dd/MM/yyyy").parse("12/11/2002"),diplome);
        d[1]=new Employer(2,"Rabe","Solo",new SimpleDateFormat("dd/MM/yyyy").parse("01/06/2000"),diplome);
        d[2]=new Employer(3,"Rafetra","Koto",new SimpleDateFormat("dd/MM/yyyy").parse("26/07/1997"),diplome);
        d[3]=this;

        ModelView result=new ModelView("Emp",d,"listeEmployer.jsp");
        return result;
    }

    @UrlServlet(url="mandebe")
    public ModelView mande(){
        System.out.println(this.id);
        return null;
    }

    public String[] getDiplome() {
        return diplome;
    }

    public void setDiplome(String[] diplome) {
        this.diplome = diplome;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }



    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }



    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public Date getDateanniv() {
        return dateanniv;
    }

    public void setDateanniv(Date dateanniv) {
        this.dateanniv = dateanniv;
    }


}
