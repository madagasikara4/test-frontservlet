create database oracle;
alter database oracle owner to postgres;
\c oracle postgres;

create table departement(
    idDept serial primary key not null,
    nom varchar(50) not null,
    localisation varchar(50) not null
);

create table employer(
    idEmp serial primary key not null,
    nom varchar(50) not null,
    prenom varchar(50) not null,
    naissance date not null,
    idDept int not null,
    foreign key (idDept) references Departement(idDept)
);

insert into departement(nom,localisation) values
('RH','Analakely');

insert into employer(nom,prenom,naissance,idDept) values ('Rabenja','Solofo','1997-05-16',1);